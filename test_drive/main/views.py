from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView, FormView

# from test_drive.main.forms import SignupForm

from django.shortcuts import render, redirect
from .forms import NewUserForm
from django.contrib.auth import login, authenticate
from django.contrib import messages


def index(request):
    return render(request, 'main/index.html')


def signup(request, backend='django.contrib.auth.backends.ModelBackend'):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            messages.success(request, "Registration successful.")
            return redirect("/levels")
        messages.error(request, "Unsuccessful registration. Invalid information.")
    form = NewUserForm()
    return render(request=request, template_name="main/signup.html", context={"register_form": form})

    # return render(request, 'main/signup.html')


def login_account(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("/levels")
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request, template_name="main/login_account.html", context={"login_form": form})

    # return render(request, 'main/login_account.html')


def levels(request):
    return render(request, 'main/levels.html')


def topics(request):
    return render(request, 'main/topics.html')


def words(request):
    return render(request, 'main/words.html')


def crossword(request):
    return render(request, 'main/crossword.html')


def check_word(request):
    if (request.GET.get('check')):
        return render(request, 'main/crossword.html')

# class RegisterForView(FormView):
#     form_class = UserCreationForm
#     success_url = "login"
#     template_name = "main/signup.html"
#
#     def form_valid(self, form):
#         form.save()
#         return super(RegisterForView, self).form_valid(form)
#
#     def form_invalid(self, form):
#         return super(RegisterForView, self).form_invalid(form)
