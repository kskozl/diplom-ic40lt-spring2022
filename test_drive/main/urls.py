from django.contrib import admin
from django.urls import path, include
from . import views
# from .views import RegisterForView

urlpatterns = [
    path('', views.index),
    path('login_account', views.login_account, name='login_account'),
    path('signup', views.signup, name='signup'),
    path('levels', views.levels),
    path('topics', views.topics),
    path('words', views.words),
    path('crossword', views.crossword),
    path('check_word', views.check_word),

    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),

]
